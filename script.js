// the url for tha api
var url = "https://www.themealdb.com/api/json/v1/1/random.php";


var recipe = "";

// will be filled in the getMeals() function
var ingredients = "";

// here we will get and fill the content
async function getMeals(){
	// get the data from the api url (wait forn response)
	const response = await fetch(url);

	// put the json response in "meals"
	const meals = await response.json();

	//this puts the json data in a outside variable because I need to use it outside the function
	recipe = meals.meals[0].strInstructions;

	// get the diffrent needed values from the json
	const mealName = meals.meals[0].strMeal;
	const mealCategory = meals.meals[0].strCategory;
	const mealImage = meals.meals[0].strMealThumb;
	const mealSource = meals.meals[0].strSource;
	const mealTags = meals.meals[0].strTags;
	

	// because meal tags are comma seperated I split them and put them in the correct ellement
	if (mealTags) {
		const tagsList =  mealTags.split(',');
		for (var i = tagsList.length - 1; i >= 0; i--) {
			console.log(tagsList[i]);
			document.getElementById('tags').innerHTML += tagsList[i] + " ";
		}
	}

	
	// these are values that have incremental names (for instance "strIngredient1, strIngredient2, etc") so collect them and put them in an array.
	var mealIngredients = [];
	var mealMeasure = [];


	// check for 20 ingredients (the max is 20) and put them in the array. If 
	for (var i = 1; i <= 20; i++) {
		var key = "strIngredient" + i;
		var ingredient = meals.meals[0][key]

		if(ingredient){
			mealIngredients.push(ingredient)
		}else{
			break;
		}
	}

	// check for 20 measures (the max is 20)
	for (var j = 1; j <= 20; j++) {
		var key = "strMeasure" + j;
		var measure = meals.meals[0][key]

		if(measure){
			mealMeasure.push(measure)
		}else{
			break;
		}
	}


	// this is a place holder outside the loop to fill
	var t = "<table>";
	for (var i = mealIngredients.length - 1; i >= 0; i--) {
		var tr = "<tr>";
      	tr += "<td>"+mealIngredients[i]+"</td>";
      	tr += "<td>"+mealMeasure[i]+"</td>"; 
      	tr += "</tr>";
      	t += tr;
	}

	// put the table in a outside variable because I need to use it outside the function
	ingredients = t;

	document.getElementById('name').innerHTML = mealName;
	document.getElementById('category').innerHTML = mealCategory;
	document.getElementById('image').src = mealImage;
	document.getElementById('source').href = mealSource;
	document.getElementById('tags').innerHTML = mealTags;

	document.getElementById('tab-content').innerHTML = recipe;
}

// triger the function that fills the ellements
getMeals();


// the onclick (switch) between recipe and ingredients
function getContent(id) {
		if (id === 'recipe') {
				document.getElementById('tab-content').innerHTML = recipe;
				document.getElementById("recipe").classList.add('active');
				document.getElementById("ingredients").classList.remove('active')

		}else if(id === 'ingredients'){
				document.getElementById('tab-content').innerHTML = ingredients;
				document.getElementById("recipe").classList.remove('active');
				document.getElementById("ingredients").classList.add('active')
		} 
}



